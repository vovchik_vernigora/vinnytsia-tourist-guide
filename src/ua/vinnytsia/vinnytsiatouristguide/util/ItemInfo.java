package ua.vinnytsia.vinnytsiatouristguide.util;

public class ItemInfo {

	public int name;
	public int[] pictures;
	//public String[] pictures; - list URIs of downloaded img from sdcard
	public int infoId;
	public double latitude;
	public double longitude;
	public String callNumber;
	public int address;
	public String email;
	public String website;
	
	public ItemInfo(int name, int infoId, int[] pictures, String callNumber, int address, String email, String website, double latitude, double longitude){
		this.name = name;
		this.pictures = pictures;
		this.infoId = infoId;
		this.callNumber = callNumber;
		this.address = address;
		this.email = email;
		this.website = website;
		this.longitude = longitude;
		this.latitude = latitude;
	}
}
