package ua.vinnytsia.vinnytsiatouristguide.util;

public class PublicVariables {
	
	public static byte LIST_ACTIVITY_TYPE = 0;
	public static byte ITEM_ACTIVITY_TYPE = 1;
	
	public static String ACTIVITY_NAME = "ActivityName";
	public static String LANG_PREF = "Language";
}
