package ua.vinnytsia.vinnytsiatouristguide.util;

public class Sell {
	
	public String name;
	public byte nextActType;
	public int nameId;
	public int imgId;
	
	public Sell(String name, int resourseId, byte nextActType, int imgId){
		this.name = name;
		this.nameId = resourseId;
		this.nextActType = nextActType;
		this.imgId = imgId;
	}
}
