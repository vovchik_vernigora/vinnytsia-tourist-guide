package ua.vinnytsia.vinnytsiatouristguide.util;

import ua.vinnytsia.vinnytsiatouristguide.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
 
public class CustomListViewAdapter extends ArrayAdapter<Sell> {
 
    Context context;
 
    public CustomListViewAdapter(Context context, int resourceId, Sell[] items) {
        super(context, resourceId, items);
        this.context = context;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        Sell rowItem = getItem(position);
        TextView txtView;
        
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.preview_form, null);
            txtView = (TextView) convertView.findViewById(R.id.textView);
            txtView.setText(context.getResources().getString(rowItem.nameId));
            txtView.setCompoundDrawablesWithIntrinsicBounds(rowItem.imgId, 0, R.drawable.forward, 0);
        } else {
        	// do nothing
        }
        
        return convertView;
    }
}

