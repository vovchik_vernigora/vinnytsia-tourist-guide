package ua.vinnytsia.vinnytsiatouristguide.util;

import java.util.HashMap;

import ua.vinnytsia.vinnytsiatouristguide.R;

public class SellList {

    private static SellList instance = null;
    private static HashMap<String, ListItemInfo> itemsList = new HashMap<String, ListItemInfo>();

    private SellList(){
        itemsList.put("Explore", new ListItemInfo(new Sell[]{new Sell("Accommodation", R.string.button_explore_accomodation, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.accomodation),
                                            new Sell("Entertaiment", R.string.button_explore_entertaiment, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.entertaiment),
                                            new Sell("See and do", R.string.button_explore_see_and_do, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.seendoo),
                                            new Sell("Tourist information centers", R.string.button_explore_tourist_info_centers, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.tourist_info_centers)}));

        itemsList.put("Tourist information", new ListItemInfo( new Sell[]{ new Sell("Contacts", R.string.button_tourist_information_contacts, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.tourist_info_centers),
                                                         new Sell("Emergency numbers", R.string.button_tourist_information_emergency_numbers, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.tourist_info_centers),
                                                         new Sell("About city", R.string.button_tourist_information_about_city, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.tourist_info_centers),
                                                         new Sell("Useful information", R.string.button_tourist_information_useful_information, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.tourist_info_centers),
                                                         new Sell("Public transport", R.string.button_tourist_information_public_transport, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.tourist_info_centers)}));

        itemsList.put("Accommodation", new ListItemInfo( new Sell[]{new Sell("Hotels", R.string.button_explore_accomodation_hotels, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.hotels),
                                                  new Sell("Hostels", R.string.button_explore_accomodation_hostels, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.hotels),
                                                  new Sell("Motels and Camps", R.string.button_explore_accomodation_motels_and_camps, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.camp),
                                                  new Sell("Dormitories", R.string.button_explore_accomodation_dormitories, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.hotels)}));
        
        itemsList.put("Hotels", new ListItemInfo( new Sell[]{new Sell("Podillya", R.string.button_explore_accomodation_hotels_podillya, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Feride.Hotel", R.string.button_explore_accomodation_hotels_feride_hotel, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Park hotel Vinnytsia", R.string.button_explore_accomodation_hotels_park_hotel_vinnytsia, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Dobrodiy", R.string.button_explore_accomodation_hotels_dobrodiy, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Zhovtnevuy", R.string.button_explore_accomodation_hotels_zhovtneviy, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Zatihok", R.string.button_explore_accomodation_hotels_zatishok, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Gostinnyu Dvir", R.string.button_explore_accomodation_hotels_gostinnuy_dvir, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Ideal", R.string.button_explore_accomodation_hotels_ideal, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Katrin", R.string.button_explore_accomodation_hotels_katrin, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Pivdennyu bug", R.string.button_explore_accomodation_hotels_pivdenniy_bug, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Status", R.string.button_explore_accomodation_hotels_status, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Versal.hotel", R.string.button_explore_accomodation_hotels_versal_hotel, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Panska hata", R.string.button_explore_accomodation_hotels_panska_hata, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Drive club.hotel", R.string.button_explore_accomodation_hotels_drive_club_hotel, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Avtoport", R.string.button_explore_accomodation_hotels_avtoport, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Ritm", R.string.button_explore_accomodation_hotels_ritm, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Straik", R.string.button_explore_accomodation_hotels_straik, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("De Luche", R.string.button_explore_entertaiment_pizza_de_luche, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Energetik", R.string.button_explore_accomodation_hotels_energetik, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Denniana", R.string.button_explore_accomodation_hotels_dennian, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Dacha", R.string.button_explore_accomodation_hotels_dacha, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                           new Sell("Vinnytsia", R.string.button_explore_accomodation_hotels_vinnytisa, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels)}));
        
        itemsList.put("Hostels", new ListItemInfo( new Sell[]{new Sell("Zhaherizada.hostel", R.string.button_explore_accomodation_hostels_zhaherizada, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                            new Sell("RIA Hostel", R.string.button_explore_accomodation_hostels_ria, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                            new Sell("Burhui.hostel", R.string.button_explore_accomodation_hostels_burshui, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                            new Sell("Safari club", R.string.button_explore_accomodation_hostels_safary_club, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels),
                                            new Sell("Towmed", R.string.button_explore_accomodation_hostels_towmed, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.hotels)}));
        
        itemsList.put("Motels and Camps", new ListItemInfo( new Sell[]{
                                                     new Sell("Klove misce", R.string.button_explore_accomodation_motels_and_camps_klove_mische, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Butterfly", R.string.button_explore_accomodation_motels_and_camps_butterfly, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Lanu", R.string.button_explore_accomodation_motels_and_camps_lanu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Zeleniy turizm", R.string.button_explore_accomodation_motels_and_camps_zeleniy_turizm, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Palamarenkovo", R.string.button_explore_accomodation_motels_and_camps_palamarenkovo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Skazka", R.string.button_explore_accomodation_motels_and_camps_skazka, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Abricos", R.string.button_explore_accomodation_motels_and_camps_abrikos, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Berezino", R.string.button_explore_accomodation_motels_and_camps_berezino, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Berizka", R.string.button_explore_accomodation_motels_and_camps_berizka, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp),
                                                     new Sell("Gostevia", R.string.button_explore_accomodation_motels_and_camps_gostevia, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.camp)}));
        
        itemsList.put("Dormitories", new ListItemInfo( new Sell[]{}));
        
        itemsList.put("Entertaiment", new ListItemInfo( new Sell[]{new Sell("Cafes", R.string.button_explore_entertaiment_cafes, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.cafes),
                                                 new Sell("Clubs", R.string.button_explore_entertaiment_clubs, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.clubs),
                                                 new Sell("Culture Events Venues", R.string.button_explore_entertaiment_culture_events_venues, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.theater),
                                                 new Sell("Leisure", R.string.button_explore_entertaiment_leisure, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.leisure),
                                                 new Sell("Pizza", R.string.button_explore_entertaiment_pizza, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.pizza),
                                                 new Sell("Pubs", R.string.button_explore_entertaiment_pubs, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.pubs),
                                                 new Sell("Restourants", R.string.button_explore_entertaiment_restourants, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.restourants),
                                                 new Sell("Shopping", R.string.button_explore_entertaiment_shopping, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.shops),
                                                 new Sell("Karaoke", R.string.button_explore_entertaiment_karaoke, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.karaoke)}));
        
        itemsList.put("Cafes", new ListItemInfo( new Sell[]{new Sell("Fashion", R.string.button_explore_entertaiment_cafes_fahsion, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Limon", R.string.button_explore_entertaiment_cafes_limon, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Pirogoff", R.string.button_explore_entertaiment_cafes_pirogoff, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Intershik", R.string.button_explore_see_and_do_galleries_intershik, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Masay mara", R.string.button_explore_entertaiment_cafes_masay_mara, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Feride.cafe", R.string.button_explore_entertaiment_cafes_feride_cafe, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Ashanty", R.string.button_explore_entertaiment_cafes_ashanty, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Bourbon", R.string.button_explore_entertaiment_cafes_bourbon, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Talisman", R.string.button_explore_entertaiment_cafes_talisman, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("KoValter", R.string.button_explore_entertaiment_cafes_kovalter, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Dom kofe", R.string.button_explore_entertaiment_cafes_dom_kofe, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Danesi", R.string.button_explore_entertaiment_cafes_danesi, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("1001+1 noch", R.string.button_explore_entertaiment_cafes_1000_1_noch, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Staroe cafe", R.string.button_explore_entertaiment_cafes_staroe_cafe, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Y fontana", R.string.button_explore_entertaiment_cafes_y_fontana, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Shatro", R.string.button_explore_entertaiment_cafes_shatro, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Zolotiy dukat", R.string.button_explore_entertaiment_cafes_zolotoy_ducat, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Slivki", R.string.button_explore_entertaiment_cafes_slivki, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Liga", R.string.button_explore_entertaiment_cafes_liga, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Tropikana", R.string.button_explore_entertaiment_cafes_tropikana, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Shokolatte", R.string.button_explore_entertaiment_cafes_shokolatte, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Knaipa", R.string.button_explore_entertaiment_cafes_knaipa, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Ice island", R.string.button_explore_entertaiment_cafes_ice_island, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Vinnytsia.cafe", R.string.button_explore_entertaiment_cafes_vinnytsia, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Y samovara", R.string.button_explore_entertaiment_cafes_y_samovara, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Borzhomi+", R.string.button_explore_entertaiment_cafes_borzhomi_plus, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("McDonalds", R.string.button_explore_entertaiment_cafes_mcdonalds, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Beer&Blues", R.string.button_explore_entertaiment_cafes_beer_n_blues, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                          new Sell("Burger club", R.string.button_explore_entertaiment_cafes_burger_club, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes)}));
        
        itemsList.put("McDonalds", new ListItemInfo( new Sell[]{new Sell("McDonalds.Soborna", R.string.button_explore_entertaiment_cafes_mcdonalds_soborna, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes),
                                              new Sell("McDonalds.Yunosti", R.string.button_explore_entertaiment_cafes_mcdonalds_yunosti, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cafes)}));
        
        itemsList.put("Clubs", new ListItemInfo( new Sell[]{new Sell("Amagama", R.string.button_explore_entertaiment_clubs_amagama, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.clubs),
                                          new Sell("H2O", R.string.button_explore_entertaiment_clubs_h2o, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.clubs),
                                          new Sell("Europe", R.string.button_explore_entertaiment_clubs_europe, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.clubs),
                                          new Sell("Planeta modabar", R.string.button_explore_entertaiment_clubs_planeta_modabar, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.clubs),
                                          new Sell("Feride.club", R.string.button_explore_entertaiment_clubs_feride, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.clubs),
                                          new Sell("Sport time", R.string.button_explore_entertaiment_clubs_sport_time, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.clubs)}));
        
        itemsList.put("Culture Events Venues", new ListItemInfo( new Sell[]{new Sell("Sadovskogo", R.string.button_explore_entertaiment_culture_events_venues_sadovskogo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.leisure),
                                                          new Sell("Pleyada", R.string.button_explore_entertaiment_culture_events_venues_pleyada, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.leisure),
                                                          new Sell("TeatrLyalok", R.string.button_explore_entertaiment_culture_events_venues_teatr_lyalok, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.leisure),
                                                          new Sell("Zorya", R.string.button_explore_entertaiment_culture_events_venues_zorya, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.leisure)}));
        
        itemsList.put("Leisure", new ListItemInfo( new Sell[]{new Sell("Adventure parks", R.string.button_explore_entertaiment_leusure_adventure_parks, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.map),
                                            new Sell("Bicycle hire", R.string.button_explore_entertaiment_leusure_bicycle_hire, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.bycicle_rent),
                                            new Sell("Billuards", R.string.button_explore_entertaiment_leusure_billiards, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.buliyards),
                                            new Sell("Bowling", R.string.button_explore_entertaiment_leusure_bowling, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.bowling),
                                            new Sell("Paintball", R.string.button_explore_entertaiment_leusure_paintball, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.aquaparcs),
                                            new Sell("Sportclubs", R.string.button_explore_entertaiment_leusure_sportclubs, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.gum),
                                            new Sell("Skating", R.string.button_explore_entertaiment_leusure_skating, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.skate),
                                            new Sell("Other.Leisure", R.string.button_explore_entertaiment_leusure_other_activityes, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.parks)}));
        
        itemsList.put("Adventure parks", new ListItemInfo( new Sell[]{}));
        itemsList.put("Bicycle hire", new ListItemInfo( new Sell[]{}));
        itemsList.put("Billuards", new ListItemInfo( new Sell[]{}));
        itemsList.put("Bowling", new ListItemInfo( new Sell[]{}));
        itemsList.put("Paintball", new ListItemInfo( new Sell[]{}));
        itemsList.put("Sportclubs", new ListItemInfo( new Sell[]{}));
        itemsList.put("Skating", new ListItemInfo( new Sell[]{}));
        itemsList.put("Other.Leisure", new ListItemInfo( new Sell[]{}));
        
        itemsList.put("Pubs", new ListItemInfo( new Sell[]{new Sell("Poyalpub", R.string.button_explore_entertaiment_pubs_royalpub, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pubs),
                                         new Sell("Mclaudpub", R.string.button_explore_entertaiment_pubs_mclaudpub, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pubs),
                                         new Sell("Nikolays", R.string.button_explore_entertaiment_pubs_nikolays, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pubs),
                                         new Sell("Y flinta", R.string.button_explore_entertaiment_pubs_y_flinta, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pubs)}));
        
        itemsList.put("Restourants", new ListItemInfo( new Sell[]{new Sell("Burhui.restourant", R.string.button_explore_entertaiment_restourants_burhui, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Fazenda", R.string.button_explore_entertaiment_restourants_fazenda, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Izum", R.string.button_explore_entertaiment_restourants_izum, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Zhaherizada.restourant", R.string.button_explore_entertaiment_restourants_zhaherizada, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Drive club.restourant", R.string.button_explore_entertaiment_restourants_drive_club, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Feride.restourant", R.string.button_explore_entertaiment_restourants_feride, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Prime", R.string.button_explore_entertaiment_restourants_prime, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Dr.Goodvin", R.string.button_explore_entertaiment_restourants_drgoodbin, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Mislivech", R.string.button_explore_entertaiment_restourants_mislivech, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Meringata", R.string.button_explore_entertaiment_restourants_meringata, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Carbon.restourant", R.string.button_explore_entertaiment_restourants_carbon, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Podkova", R.string.button_explore_entertaiment_restourants_podkova, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Kasablanka", R.string.button_explore_entertaiment_restourants_kasablanka, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("City", R.string.button_explore_entertaiment_restourants_city, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Kozackiy stan.restourant", R.string.button_explore_entertaiment_restourants_kozackiy_stan, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Steik house", R.string.button_explore_entertaiment_restourants_steik_house, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Versal.restourant", R.string.button_explore_entertaiment_restourants_versal, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Vodogray", R.string.button_explore_entertaiment_restourants_vodogray, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Stariy york", R.string.button_explore_entertaiment_restourants_stariy_york, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Panska hata", R.string.button_explore_entertaiment_restourants_panska_hata, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Nachional", R.string.button_explore_entertaiment_restourants_nachional, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Anastasia", R.string.button_explore_entertaiment_restourants_anastasia, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Provans", R.string.button_explore_entertaiment_restourants_provans, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Slovanskiy dvir", R.string.button_explore_entertaiment_restourants_slovanskiy_dvir, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Shanhay", R.string.button_explore_entertaiment_restourants_shanhai, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Med podillya", R.string.button_explore_entertaiment_restourants_med_podillya, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Avtoport.restourant", R.string.button_explore_entertaiment_restourants_avtoport, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Vagnes", R.string.button_explore_entertaiment_restourants_vagnes, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Platinum", R.string.button_explore_entertaiment_restourants_platinum, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("DreamHouse", R.string.button_explore_entertaiment_restourants_dreamhouse, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("La terazza", R.string.button_explore_entertaiment_restourants_la_terazza, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Mafia", R.string.button_explore_entertaiment_restourants_mafia, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants),
                                                new Sell("Faraon", R.string.button_explore_entertaiment_restourants_faraon, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.restourants)}));
        
        itemsList.put("Shopping", new ListItemInfo( new Sell[]{}));
        
        itemsList.put("Pizza", new ListItemInfo( new Sell[]{new Sell("La terazza", R.string.button_explore_entertaiment_pizza_la_terazza, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pizza),
                                          new Sell("Beatriche", R.string.button_explore_entertaiment_pizza_beatriche, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.pizza),
                                          new Sell("Teramare", R.string.button_explore_entertaiment_pizza_teramare, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.pizza),
                                          new Sell("Chelentano", R.string.button_explore_entertaiment_pizza_chelentano, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.pizza),
                                          new Sell("De Luche", R.string.button_explore_entertaiment_pizza_de_luche, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pizza),
                                          new Sell("NewYork", R.string.button_explore_entertaiment_pizza_newyork, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.pizza)}));
        
        itemsList.put("Beatriche", new ListItemInfo( new Sell[]{new Sell("Beatriche.porika", R.string.button_explore_entertaiment_pizza_beatriche_porika, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pizza),
                                              new Sell("Beatriche.podol", R.string.button_explore_entertaiment_pizza_beatriche_podol, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pizza)}));
        
        itemsList.put("Teramare", new ListItemInfo( new Sell[]{new Sell("Teramare.kocubinskogo", R.string.button_explore_entertaiment_pizza_teramare_kocubinskogo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pizza),
                                             new Sell("Teramare.podolie", R.string.button_explore_entertaiment_pizza_teramare_kocubinskogo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pizza),
                                             new Sell("Teramare.kelichka", R.string.button_explore_entertaiment_pizza_teramare_kelichka, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.pizza)}));
        
        itemsList.put("Chelentano", new ListItemInfo( new Sell[]{}));
        itemsList.put("NewYork",new ListItemInfo(  new Sell[]{}));
        
        itemsList.put("Karaoke",new ListItemInfo( new Sell[]{new Sell("Carbon", R.string.button_explore_entertaiment_karaoke_carbon, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.karaoke),
                                            new Sell("Mafia", R.string.button_explore_entertaiment_restourants_mafia, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.karaoke)}));
        
        itemsList.put("See and do",new ListItemInfo( new Sell[]{new Sell("Top 10", R.string.button_explore_see_and_do_top_10, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.top10),
                                                  new Sell("Galleries", R.string.button_explore_see_and_do_galleries, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.galleries),
                                                  new Sell("Handicraft Workshops", R.string.button_explore_see_and_do_handicraft_workshop, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.workshops),
                                                  new Sell("Monuments", R.string.button_explore_see_and_do_monuments, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.monuments),
                                                  new Sell("Museums", R.string.button_explore_see_and_do_museums, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.museums),
                                                  new Sell("Parks", R.string.button_explore_see_and_do_parks, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.parks),
                                                  new Sell("Places of Interest", R.string.button_explore_see_and_do_places_of_interest, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.places_of_interest),
                                                  new Sell("Spiritual", R.string.button_explore_see_and_do_spiral_structures, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.churches),
                                                  new Sell("Tours", R.string.button_explore_see_and_do_tours, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.tourist_routs),
                                                  new Sell("Viewpoints", R.string.button_explore_see_and_do_viewpoints, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.seendoo),
                                                  new Sell("Millitary", R.string.button_explore_see_and_do_millitary, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.military),
                                                  new Sell("Cinemas", R.string.button_explore_see_and_do_cinemas, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.cinema),
                                                  new Sell("OutFromTheCity", R.string.button_explore_see_and_do_out_from_the_city, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.map),
                                                  new Sell("Extra", R.string.button_explore_see_and_do_extra, PublicVariables.LIST_ACTIVITY_TYPE, R.drawable.extra)}));
        
        itemsList.put("Top 10",new ListItemInfo( new Sell[]{}));
        itemsList.put("Galleries",new ListItemInfo( new Sell[]{new Sell("Intershik", R.string.button_explore_see_and_do_galleries_intershik, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.galleries),
                                              new Sell("Megamoll", R.string.button_explore_see_and_do_galleries_megamoll, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.galleries),
                                              new Sell("Muzey mistactva", R.string.button_explore_see_and_do_galleries_muzey_mistactva, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.galleries)
                                              /*new Sell("bila podillya", R.string.button_explore_entertaiment, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.vinnitsa),
                                              new Sell("teatrakmna*pershotravneva", R.string.button_explore_entertaiment, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.vinnitsa),*/}));
        
        itemsList.put("Spiritual",new ListItemInfo( new Sell[]{}));
        itemsList.put("Handicraft Workshops",new ListItemInfo( new Sell[]{}));
        
        itemsList.put("Monuments",new ListItemInfo( new Sell[]{new Sell("chegevari", R.string.button_explore_see_and_do_monuments_chegevari, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("bokseru", R.string.button_explore_see_and_do_monuments_bokseru, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("papi rumskomu ioanu-pavlu-2", R.string.button_explore_see_and_do_monuments_papi_rumskomu_ioanu_pavlu_2, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("artinovu", R.string.button_explore_see_and_do_monuments_artinovu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("gorkomu", R.string.button_explore_see_and_do_monuments_gorkomu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("zagiblim milicioneram", R.string.button_explore_see_and_do_monuments_zagiblim_milicioneram, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("serdche", R.string.button_explore_see_and_do_monuments_serdche, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("zhertvam-chormobilcam", R.string.button_explore_see_and_do_monuments_zhertvam_chormobilcam, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("vinnytskiy tramvay", R.string.button_explore_see_and_do_monuments_vinnytskiy_tramvay, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("pamatnik pisni", R.string.button_explore_see_and_do_monuments_pamatnik_pisni, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("avgancam", R.string.button_explore_see_and_do_monuments_avgancam, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("bevzu", R.string.button_explore_see_and_do_monuments_bevzu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("memorial slavi"/*v parku*/, R.string.button_explore_see_and_do_monuments_memorial_slavi, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("tarasu shevchenku", R.string.button_explore_see_and_do_monuments_tarasu_shevchenku, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("kocubinskomu", R.string.button_explore_see_and_do_monuments_kocubinskomu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("litak.kosmonavtiv", R.string.button_explore_see_and_do_monuments_litak_kosmonavtiv, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("litak.kocubinskogo", R.string.button_explore_see_and_do_monuments_litak_kocubinskogo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("zertvam golodomoru", R.string.button_explore_see_and_do_monuments_zertvam_golodomory, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("bogumu", R.string.button_explore_see_and_do_monuments_bogunu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("stusu", R.string.button_explore_see_and_do_monuments_stusu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("pirogova", R.string.button_explore_see_and_do_monuments_pirogova, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("tarnagrodskogo", R.string.button_explore_see_and_do_monuments_tarnagrodskogo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("frunze", R.string.button_explore_see_and_do_monuments_frunze, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments),
                                              new Sell("gagarimu", R.string.button_explore_see_and_do_monuments_gagarimu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.monuments)}));
        
        itemsList.put("Museums",new ListItemInfo( new Sell[]{new Sell("Kraeznavchiy muzey", R.string.button_explore_see_and_do_museums_kraeznavchiy, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            //new Sell("Muzeu shokoladu roshen", R.string.button_explore_see_and_do_museums_shokoladu, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.vinnitsa),
                                            new Sell("Muzeu raketnih viysk", R.string.button_explore_see_and_do_museums_raketnih_viusk, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Avtomotovelofotoradio", R.string.button_explore_see_and_do_museums_avtomotovelofotoradio, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Myzeu zbroi kolo stavki gitlera", R.string.button_explore_see_and_do_out_from_the_city_muzyu_zbroi_kolo_stavki_gitlera, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Myzeu kocubinskogo", R.string.button_explore_see_and_do_museums_kocubinckogo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Myzeu pirogova", R.string.button_explore_see_and_do_museums_pirogova, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Kartinu v politehi", R.string.button_explore_see_and_do_museums_kartinu_v_politehi, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Myzeu pozhezhnoi slavi", R.string.button_explore_see_and_do_museums_pozhezhnoi_slavi, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Ysupalnica pirogova", R.string.button_explore_see_and_do_museums_ysipalnich_pirogova, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            //new Sell("Myzeu na electromerezhi", R.string.button_explore_see_and_do_museums_, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.vinnitsa),
                                            new Sell("Myzeu avganchiv v bashni", R.string.button_explore_see_and_do_museums_avganciv, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Myzeu anatomii d medynivery", R.string.button_explore_see_and_do_museums_anatomii, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Myzeu tramvaiv", R.string.button_explore_see_and_do_museums_tramvaiv, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums),
                                            new Sell("Myzeu hudozhiniy", R.string.button_explore_see_and_do_museums_hudojnii, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.museums)}));
        
        itemsList.put("OutFromTheCity",new ListItemInfo( new Sell[]{new Sell("Stavka gitlera", R.string.button_explore_see_and_do_out_from_the_city_stavka_gitlera, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.map),
                                                 new Sell("palac goroholskih", R.string.button_explore_see_and_do_out_from_the_city_palac_goroholskih, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.map),
                                                 new Sell("Palach Potochkih", R.string.button_explore_see_and_do_out_from_the_city_palach_potochkih, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.map),
                                                 new Sell("Gaydamachkiy yar", R.string.button_explore_see_and_do_out_from_the_city_gaydamachkiy_yar, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.map),
                                                 new Sell("Busha", R.string.button_explore_see_and_do_out_from_the_city_busha, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.map),
                                                 new Sell("Lyadovskiy monastir", R.string.button_explore_see_and_do_out_from_the_city_lyadovskiy_monastir, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.map),
                                                 new Sell("Myzeu zbroi kolo stavki gitlera", R.string.button_explore_see_and_do_out_from_the_city_muzyu_zbroi_kolo_stavki_gitlera, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.map)}));
        
        itemsList.put("Millitary",new ListItemInfo( new Sell[]{new Sell("Buneker voroshilova", R.string.button_explore_see_and_do_millitary_bunker_vorohilova, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.military),
                                             new Sell("Stavka gitlera", R.string.button_explore_see_and_do_out_from_the_city_stavka_gitlera, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.military),
                                             new Sell("Avtomotovelofotoradio", R.string.button_explore_see_and_do_museums_avtomotovelofotoradio, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.military),
                                             new Sell("Muzeu raketnih viysk", R.string.button_explore_see_and_do_museums_raketnih_viusk, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.military),
                                             new Sell("Bunker pvo", R.string.button_explore_see_and_do_millitary_buker_pvo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.military),
                                             new Sell("Myzeu zbroi kolo stavki gitlera", R.string.button_explore_see_and_do_out_from_the_city_muzyu_zbroi_kolo_stavki_gitlera, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.military)}));
        
        itemsList.put("Places of Interest",new ListItemInfo( new Sell[]{new Sell("arhiv", R.string.button_explore_see_and_do_places_of_interest_arhiv, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo)}));
        
        itemsList.put("Tours",new ListItemInfo( new Sell[]{}));
        
        itemsList.put("Viewpoints",new ListItemInfo( new Sell[]{new Sell("Fontan", R.string.button_explore_see_and_do_viewpoints_fontan, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo),
                                               new Sell("Bahnya", R.string.button_explore_see_and_do_viewpoints_bashnya, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo),
                                               new Sell("Vhid v park", R.string.button_explore_see_and_do_viewpoints_vhid_v_park, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo),
                                               new Sell("Dom Generala", R.string.button_explore_see_and_do_viewpoints_sabariv, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo),
                                               new Sell("IloveVinnytsia", R.string.button_explore_see_and_do_viewpoints_i_love_vinnytsia, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo),
                                               new Sell("Panorama na bevza", R.string.button_explore_see_and_do_viewpoints_panorama_na_bevza, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo),
                                               new Sell("Sabariv", R.string.button_explore_see_and_do_viewpoints_sabariv, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo),
                                               new Sell("Savoy", R.string.button_explore_see_and_do_viewpoints_savoy, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.seendoo)}));
        
        itemsList.put("Extra",new ListItemInfo( new Sell[]{}));
        
        itemsList.put("Parks",new ListItemInfo( new Sell[]{new Sell("Centralniy park", R.string.button_explore_see_and_do_parks_centralniy, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.parks),
                                          new Sell("Park druzhbi narodiv", R.string.button_explore_see_and_do_parks_drujbi_narodiv, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.parks),
                                          new Sell("Lisopark", R.string.button_explore_see_and_do_parks_lisopark, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.parks),
                                          new Sell("Park pirogova", R.string.button_explore_see_and_do_parks_pirogova, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.parks)}));
        
        itemsList.put("Cinemas",new ListItemInfo( new Sell[]{new Sell("Mir", R.string.button_explore_see_and_do_cinemas_mir, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cinema),
                                            new Sell("Kochubinskogo", R.string.button_explore_see_and_do_cinemas_kocubinskogo, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cinema),
                                            new Sell("Rodina", R.string.button_explore_see_and_do_cinemas_rodina, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.cinema)}));
        
        itemsList.put("Tourist information centers",new ListItemInfo( new Sell[]{new Sell("Info center in basna", R.string.button_explore_tourist_information_centers_bahnya, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.info),
                                                                new Sell("Info center in teapot", R.string.button_explore_tourist_information_centers_teapot, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.info)}));
    }

    public static SellList getInstance(){
        if (instance == null){
            instance = new SellList();
        }
        return instance;
    }

    public ListItemInfo getSells(String name){
        return itemsList.get(name);
    }
}
