package ua.vinnytsia.vinnytsiatouristguide.util;

public class ListItemInfo {

	public int[] imgsId;
	public Sell[] sells;
	
	public ListItemInfo(Sell[] sells, int[] imgsId){
		this.sells = sells;
		this.imgsId = imgsId;
	}
	
	public ListItemInfo(Sell[] sells){
		this.sells = sells;
		imgsId = null;
	}
	
}
