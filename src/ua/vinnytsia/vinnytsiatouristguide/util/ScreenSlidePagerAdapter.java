package ua.vinnytsia.vinnytsiatouristguide.util;

import ua.vinnytsia.vinnytsiatouristguide.R;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    private int[] imgIds;
    
    public ScreenSlidePagerAdapter(FragmentManager fm, int[] imgsId) {
        super(fm);
        if (imgsId != null){
            this.imgIds = imgsId;
        } else {
            this.imgIds = new int[]{R.drawable.noimg};
        }
    }

    @Override
    public Fragment getItem(int position) {
        return ScreenSlidePageFragment.create(imgIds[position]);
    }

    @Override
    public int getCount() {
        return imgIds.length;
    }
}
