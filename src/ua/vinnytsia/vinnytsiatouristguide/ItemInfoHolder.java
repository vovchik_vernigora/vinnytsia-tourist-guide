package ua.vinnytsia.vinnytsiatouristguide;

import ua.vinnytsia.vinnytsiatouristguide.util.ItemInfo;
import ua.vinnytsia.vinnytsiatouristguide.util.ItemsView;
import ua.vinnytsia.vinnytsiatouristguide.util.PublicVariables;
import ua.vinnytsia.vinnytsiatouristguide.util.ScreenSlidePagerAdapter;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemInfoHolder extends Activity {
    private String activityName;
    private ItemInfo infoItem;
    
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_info_holder);

        Bundle b = getIntent().getExtras();
        activityName = b.getString(PublicVariables.ACTIVITY_NAME);
        
        infoItem = ItemsView.getInstance().getItemInfo(activityName);
        
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager(), infoItem.pictures);
        mPager.setAdapter(mPagerAdapter);
        
        ((TextView)findViewById(R.id.itemName)).setText(getResources().getString(infoItem.name));
        ((TextView)findViewById(R.id.itemDesc)).setText(getResources().getString(infoItem.infoId));
        
        if (infoItem.callNumber != null){
            turnOnCallBtn();
        } else {
            turnOffCallBtn();
        }
        
        if (infoItem.website != null){
            turnOnWWWBtn();
        } else {
            turnOffWWWBtn();
        }

        if (infoItem.email != null){
            turnOnEmailBtn();
        } else {
            turnOffEmailBtn();
        }
        
        if (infoItem.latitude != 0){
        	turnOnRouteToBtn();
        } else {
        	turnOffRouteToBtn();
        }
        
        String tmpStr = "" + (getResources().getString(infoItem.address) == "" ? "" : "address: " + getResources().getString(infoItem.address) + "\n") 
                           + (infoItem.callNumber == null ? "" : "callNumber: " + infoItem.callNumber + "\n")
                           + (infoItem.website == null ? "" : "website: " + infoItem.website + "\n")
                           + (infoItem.email == null ? "" : "email: " + infoItem.email + "\n");
        ((TextView)findViewById(R.id.additionalInfo)).setText(tmpStr);
    }
    
    private void turnOnCallBtn(){
        Button btn = (Button) findViewById(R.id.itemCallBtn);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + infoItem.callNumber)));
            }
        });
    }
    private void turnOffCallBtn(){
        Button btn = (Button) findViewById(R.id.itemCallBtn);
        btn.setEnabled(false);
    }
    
    private void turnOnWWWBtn(){
        Button btn = (Button) findViewById(R.id.itemWebBtn);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(infoItem.website)));
            }
        });
    }
    private void turnOffWWWBtn(){
        Button btn = (Button) findViewById(R.id.itemWebBtn);
        btn.setEnabled(false);
    }
    
    private void turnOnEmailBtn(){
        Button btn = (Button) findViewById(R.id.itemEmailToBtn);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            	String[] recipients = new String[]{infoItem.email};
            	emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
            	emailIntent.setType("text/plain");
            	startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });
    }
    private void turnOffEmailBtn(){
        Button btn = (Button) findViewById(R.id.itemEmailToBtn);
        btn.setEnabled(false);
    }
    
    private void turnOnRouteToBtn(){
        Button btn = (Button) findViewById(R.id.itemRouteToBtn);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW,  Uri.parse("geo:0,0?q="+ infoItem.latitude +"," + infoItem.longitude + "(" + getResources().getString(infoItem.name) + ")")));
            }
        });
    }
    private void turnOffRouteToBtn(){
        Button btn = (Button) findViewById(R.id.itemRouteToBtn);
        btn.setEnabled(false);
    }
    
    private class ImageAdapter extends BaseAdapter {
        private Context context;
        private int[] picts;

        ImageAdapter(Context context, int[] picts) {
            this.context = context;
            this.picts = picts;
        }

        public int getCount() {
            return picts.length;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // Not using convertView for simplicity. You should probably use it in real application to get better performance.
            ImageView imageView = new ImageView(context);
            //int resId = picts[position];
            imageView.setImageResource(picts[position]);
            return imageView;
        }
    }
}


