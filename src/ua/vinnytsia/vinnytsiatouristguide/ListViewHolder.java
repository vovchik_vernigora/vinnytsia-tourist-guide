package ua.vinnytsia.vinnytsiatouristguide;

import ua.vinnytsia.vinnytsiatouristguide.util.CustomListViewAdapter;
import ua.vinnytsia.vinnytsiatouristguide.util.ItemsView;
import ua.vinnytsia.vinnytsiatouristguide.util.ListItemInfo;
import ua.vinnytsia.vinnytsiatouristguide.util.PublicVariables;
import ua.vinnytsia.vinnytsiatouristguide.util.ScreenSlidePagerAdapter;
import ua.vinnytsia.vinnytsiatouristguide.util.Sell;
import ua.vinnytsia.vinnytsiatouristguide.util.SellList;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class ListViewHolder extends Activity {
	
	private String activityName;
	private Context context = null;
	private Sell[] sells;
	private ListItemInfo listItemInfo;
    private int[] imgsId;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        Bundle b = getIntent().getExtras();
        activityName = b.getString(PublicVariables.ACTIVITY_NAME);
        
        listItemInfo = SellList.getInstance().getSells(activityName);
        sells = listItemInfo.sells;
        
        imgsId = listItemInfo.imgsId;
        if (imgsId != null){
        	imgsId = ItemsView.getInstance().getImgsId(getSellsNames(sells));
        }
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager(), imgsId);
        mPager.setAdapter(mPagerAdapter);
        
		ListView listView = (ListView) findViewById(R.id.sellListView);
		listView.setAdapter(new CustomListViewAdapter(context,
				android.R.layout.simple_list_item_1, sells));
        
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                Sell selectedSell = sells[myItemInt];
                
                Intent intent;
                if (selectedSell.nextActType == PublicVariables.LIST_ACTIVITY_TYPE){
                    intent = new Intent(ListViewHolder.this, ListViewHolder.class);
                } else {
                    intent = new Intent(ListViewHolder.this, ItemInfoHolder.class);
                }
                
                Bundle b = new Bundle();
                b.putString(PublicVariables.ACTIVITY_NAME, selectedSell.name);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }
	
	private String[] getSellsNames(Sell[] sells){
		String[] names = new String[sells.length];
		for (int i = 0; i < sells.length; i++){
			names[i] = sells[i].name;
		}
		return names;
	}
}
