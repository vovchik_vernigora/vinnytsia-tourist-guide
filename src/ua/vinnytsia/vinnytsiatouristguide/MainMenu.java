package ua.vinnytsia.vinnytsiatouristguide;

import java.util.Locale;

import ua.vinnytsia.vinnytsiatouristguide.MapNavigation;
import ua.vinnytsia.vinnytsiatouristguide.util.PublicVariables;
import ua.vinnytsia.vinnytsiatouristguide.util.ScreenSlidePagerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainMenu extends Activity {

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private Locale myLocale;
    private Button mapNavigationButton;
    private Button exploreButton;
    private Button touristInfoButton;
    private Button settingsButton;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        loadLocale();
        
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager(), new int[]{R.drawable.vin1,R.drawable.vin2,R.drawable.vin3,R.drawable.vin4,R.drawable.vin5,});
        mPager.setAdapter(mPagerAdapter);
        
        mapNavigationButton = (Button)findViewById(R.id.MapNavigationButton);
        mapNavigationButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainMenu.this, MapNavigation.class);
                startActivity(intent);
            }
        });
        exploreButton = (Button)findViewById(R.id.ExploreButton);
        exploreButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                lunchCustomListActuvuty("Explore");
            }
        });
        
        touristInfoButton = (Button)findViewById(R.id.TouristInfoButton);
        touristInfoButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                lunchCustomListActuvuty("Tourist information");
            }
        });
        
        settingsButton = (Button)findViewById(R.id.SettingsButton);
        settingsButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainMenu.this, LanguageCheckActivity.class);
                startActivity(intent);
            }
        });
        setButtonsText();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        loadLocale();
        setButtonsText();
    }
    
    private void setButtonsText() {
    	mapNavigationButton.setText(R.string.button_main_menu_map_navigation);
    	exploreButton.setText(R.string.button_main_menu_explore);
    	touristInfoButton.setText(R.string.button_main_menu_tourist_information);
    	settingsButton.setText(R.string.button_main_menu_settings);
    }
    
    private void lunchCustomListActuvuty(String nextActivityName) {
        Intent intent = new Intent(MainMenu.this, ListViewHolder.class);
        Bundle b = new Bundle();
            b.putString(PublicVariables.ACTIVITY_NAME, nextActivityName);
        intent.putExtras(b);
        startActivity(intent);
    }
    
    public void loadLocale()
    {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        changeLang(language);
    }
    
	public void changeLang(String lang)
	{
	    if (lang.equalsIgnoreCase("")){
	        return;
	    }
	    System.out.println("new lang = " + lang);
	    myLocale = new Locale(lang);
	    Locale.setDefault(myLocale);
	    android.content.res.Configuration config = new android.content.res.Configuration();
	    config.locale = myLocale;
	    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	}
}
