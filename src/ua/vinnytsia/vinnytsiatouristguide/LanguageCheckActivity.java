package ua.vinnytsia.vinnytsiatouristguide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ua.vinnytsia.vinnytsiatouristguide.util.PublicVariables;
import ua.vinnytsia.vinnytsiatouristguide.util.Sell;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class LanguageCheckActivity extends Activity {

	private Context context = null;
	private Locale myLocale;
	
	private class LanguagesList{
		
		int[] buttonNames = new int[]{R.string.lang_eng, R.string.lang_ua, R.string.lang_rus};
		String[] languageKeys = new String[]{"en","ua","ru"};
		
		public LanguagesList(){
		}
		
		public List<String> getButtonListOfLangs(){
			List<String> result = new ArrayList<String>();
			for(int key : buttonNames){
				result.add(context.getResources().getString(key));
			}
			return result;
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        context = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_language_check);
		
		final LanguagesList list = new LanguagesList();
		
		List<String> listOfLanguages = list.getButtonListOfLangs();
		
		ListView listView = (ListView) findViewById(R.id.langListView);
		listView.setAdapter(new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1, listOfLanguages));
		
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
            	changeLang(list.languageKeys[myItemInt]);
            }
        });
	}
	
	public void changeLang(String lang)
	{
	    if (lang.equalsIgnoreCase("")){
	     return;
	    }
	    System.out.println("new lang = " + lang);
	    myLocale = new Locale(lang);
	    saveLocale(lang);
	    Locale.setDefault(myLocale);
	    android.content.res.Configuration config = new android.content.res.Configuration();
	    config.locale = myLocale;
	    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	}
	
	public void saveLocale(String lang)
	{
	    SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PublicVariables.LANG_PREF, lang);
	    editor.commit();
	}
}
