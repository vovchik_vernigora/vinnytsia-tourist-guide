package ua.vinnytsia.vinnytsiatouristguide;

import java.util.Stack;

import ua.vinnytsia.vinnytsiatouristguide.util.CustomListViewAdapter;
import ua.vinnytsia.vinnytsiatouristguide.util.ItemInfo;
import ua.vinnytsia.vinnytsiatouristguide.util.ItemsView;
import ua.vinnytsia.vinnytsiatouristguide.util.ListItemInfo;
import ua.vinnytsia.vinnytsiatouristguide.util.PublicVariables;
import ua.vinnytsia.vinnytsiatouristguide.util.ScreenSlidePagerAdapter;
import ua.vinnytsia.vinnytsiatouristguide.util.Sell;
import ua.vinnytsia.vinnytsiatouristguide.util.SellList;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapNavigation extends FragmentActivity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	private SupportMapFragment mapFragment;
	private GoogleMap map;
	private LocationClient mLocationClient;
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private Context context = null;
	private final String firstElement = "Explore";
	private Sell[] sells;
	
	private Stack<String> listOfMenus = new Stack<String>();
	/*
	 * Define a request code to send to Google Play services This code is
	 * returned in Activity.onActivityResult
	 */
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	// Define a DialogFragment that displays the error dialog
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		// Default constructor. Sets the dialog field to null
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		// Set the dialog to display
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		// Return a Dialog to the DialogFragment.
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        context = getApplicationContext();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_navigation);

		mLocationClient = new LocationClient(this, this, this);

		mapFragment = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map));
		map = mapFragment.getMap();

		map.setMyLocationEnabled(true);
		
		listOfMenus.add(firstElement);
		makeMenu();
		addMarkers();
	}
	
	private void addMarkers(){
		map.clear();
		if(sells != null) {
			for(Sell key : sells) {
				if (key != null && key.nextActType == PublicVariables.ITEM_ACTIVITY_TYPE) {
					ItemInfo element = ItemsView.getInstance().getItemInfo(key.name);
					if(element != null) { 
						if (element.latitude != 0 && element.longitude != 0) {
							putMarker(element.latitude,element.longitude,getResources().getString(element.name));
						}
					}
				}
			}
		}
	}

	private void putMarker(double latitude, double longitude, String name){
        map.addMarker(new MarkerOptions()
        .position(new LatLng(latitude, longitude))
        .title(name));
	}
	/*
	 * Called when the Activity becomes visible.
	 */
	@Override
	protected void onStart() {
		super.onStart();
		// Connect the client.
		if (isGooglePlayServicesAvailable()) {
			mLocationClient.connect();
		}
	}

	/*
	 * Called when the Activity is no longer visible.
	 */
	@Override
	protected void onStop() {
		// Disconnecting the client invalidates it.
		mLocationClient.disconnect();
		super.onStop();
	}

	/*
	 * Handle results returned to the FragmentActivity by Google Play services
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Decide what to do based on the original request code
		switch (requestCode) {

		case CONNECTION_FAILURE_RESOLUTION_REQUEST:
			/*
			 * If the result code is Activity.RESULT_OK, try to connect again
			 */
			switch (resultCode) {
			case Activity.RESULT_OK:
				mLocationClient.connect();
				break;
			}

		}
	}

	private boolean isGooglePlayServicesAvailable() {
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			//Log.d("Location Updates", "Google Play services is available.");
			return true;
		} else {
			// Get the error dialog from Google Play services
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
					resultCode, this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

			// If Google Play services can provide an error dialog
			if (errorDialog != null) {
				// Create a new DialogFragment for the error dialog
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(errorDialog);
				errorFragment.show(getSupportFragmentManager(),
						"Location Updates");
			}

			return false;
		}
	}

	/*
	 * Called by Location Services when the request to connect the client
	 * finishes successfully. At this point, you can request the current
	 * location or start periodic updates
	 */
	@Override
	public void onConnected(Bundle dataBundle) {
		// Display the connection status
		Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
		Location location = mLocationClient.getLastLocation();
		LatLng latLng = new LatLng(location.getLatitude(),
				location.getLongitude());
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,15);
		map.animateCamera(cameraUpdate);
	}

	/*
	 * Called by Location Services if the connection to the location client
	 * drops because of an error.
	 */
	@Override
	public void onDisconnected() {
		// Display the connection status
		Toast.makeText(this, "Disconnected. Please re-connect.",
				Toast.LENGTH_SHORT).show();
	}

	/*
	 * Called by Location Services if the attempt to Location Services fails.
	 */
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
		 * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this,
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */
			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		} else {
			Toast.makeText(getApplicationContext(),
					"Sorry. Location services not available to you",
					Toast.LENGTH_LONG).show();
		}
	}
	
	private Sell[] getMenuList(String listName) {
		sells = addFirstSell(SellList.getInstance().getSells(listName).sells);
		return sells;
	}
	
    private Sell[] addFirstSell(Sell[] sells) {
        Sell[] result;
        if (listOfMenus.size() > 1) {
            result = new Sell[sells.length + 1]; //+1 for BACK BUTTON
            result[0] = new Sell("Back", R.string.map_navigation_back, PublicVariables.ITEM_ACTIVITY_TYPE, R.drawable.forward);
            
            for(int i=1; i<sells.length + 1; i++) { //+1 for BACK BUTTON
                result[i] = sells[i - 1]; //1 for BACK BUTTON
            }
        } else {
            result = new Sell[sells.length];
            for(int i=0; i<sells.length; i++) { //+1 for BACK BUTTON
                result[i] = sells[i]; //1 for BACK BUTTON
            }
        }
        return result;
    }
    
	private void makeMenu() {
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_elements_list);
		mDrawerList = (ListView) findViewById(R.id.left_drawer_elements_list);
		
		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// set up the drawer's list view with items and click listener
		mDrawerList.setAdapter(new CustomListViewAdapter(context,
				android.R.layout.simple_list_item_1, getMenuList(listOfMenus.peek())));
        
		/*mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, getMenuList(listOfMenus.peek())));*/
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		mDrawerToggle = new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				)
				{
					public void onDrawerClosed(View view) {
						invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
					}
					public void onDrawerOpened(View drawerView) {
						invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
					}
				};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}
	
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        if (listOfMenus.size() > 1) {
            if (position == 0) {
                listOfMenus.pop();
                makeMenu();
                addMarkers();
            } else {
                putMarkersAndMoveToNextSelectedItem(position);
            }
        } else {
            putMarkersAndMoveToNextSelectedItem(position);
        }
    }
    
    private void putMarkersAndMoveToNextSelectedItem(int position){
    	if (sells[position].nextActType == PublicVariables.LIST_ACTIVITY_TYPE) {
            listOfMenus.add(sells[position].name);
            makeMenu();
            addMarkers();
    	}
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
	
}